# Clap Walkthrough

Documents via commits how to implement different command line argument parsing features using Clap.

- positional arguments           ,[f125939 feat: positional arguments]
- optional positional arguments  ,[f630762 feat: optional positional arguments]
- capture all remaining argument ,[fd5236c feat: capture all remaining argument]
- flags                          ,[3b00f64 feat: Add flags]
- named argument                 ,[5790849 feat: Named arguments]
- sub commands

