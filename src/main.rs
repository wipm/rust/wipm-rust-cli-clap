use clap::{Parser, Subcommand};

#[derive(Parser, Debug)]
struct Cli {
    name: Option<String>,
    #[arg(long)]
    no_parser: bool,
    run: Vec<String>,
    #[arg(long, short)]
    title: Option<String>,
    #[command(subcommand)]
    command: Option<Commands>,
}

#[derive(Subcommand, Debug)]
enum Commands {
    Get(GetArgs),
    Post,
    Patch,
}

#[derive(Parser, Debug)]
struct GetArgs {
    resource: String,
}

fn main() {
    let args = Cli::parse();

    if let Some(n) = args.name {
        println!("Hello {}", n);
    }

    if let Some(c) = args.command {
        match c {
            Commands::Get(a) => {
                println!("Get {}", a.resource);
            }
            Commands::Post => println!("Post"),
            Commands::Patch => println!("Patch"),
        }
    }

    if args.no_parser {
        println!("No parser");
    }

    for r in &args.run {
        println!("Run: {}", r);
    }

    if let Some(t) = args.title {
        println!("Title: {}", t);
    }
}
